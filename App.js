import React from 'react';
import { Button } from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createDrawerNavigator } from '@react-navigation/drawer';

import CustomSidebarMenu from './screens/customSideBarMenu';
import onBoarding from './screens/onBoarding';
import Login from './screens/LoginScreen';
import Register from './screens/RegisterScreen'
import HomeScreen from './screens/HomeScreen';
import SearchScreen from './screens/SearchScreen';
import RestaurantScreen from './screens/RestaurantScreen';
import FoodOption from './screens/FoodDetailOptionScreen';

import SplashScreen from 'react-native-splash-screen';
import { GoogleSignin } from '@react-native-google-signin/google-signin';
// import AsyncStorage from '@react-native-async-storage/async-storage';

import * as eva from '@eva-design/eva';
import { ApplicationProvider, IconRegistry, Avatar } from '@ui-kitten/components';
import { EvaIconsPack } from '@ui-kitten/eva-icons';
import IonIcon from 'react-native-vector-icons/Ionicons';

const Stack = createNativeStackNavigator();
const Drawer = createDrawerNavigator();
 
const DrawerNavigator = ({navigation}) =>{
  return(
    <Drawer.Navigator drawerContent={(props) => <CustomSidebarMenu {...props} />}>
      {/* Drawer First 1 is initial route */}
      <Drawer.Screen name="FoodMan" component={HomeScreen} options={{headerRight: () => (
            <IonIcon name="md-search-circle-sharp" size={35} style={{marginRight:"5%"}} onPress={()=>{navigation.navigate("Search")}}/>
          ) }} />
      <Drawer.Screen name="Register" component={Register} />
      <Drawer.Screen name="Login" component={Login} />
    </Drawer.Navigator>
  )
}

class App extends React.Component{
  state = {
    isFirstLaunch:null
  }
  
  componentDidMount(){
    SplashScreen.hide();
    GoogleSignin.configure({
      webClientId: '934217778109-sljv1v2dtnvcbls5supg5u1n0jjkpoie.apps.googleusercontent.com',
    });
    
    // AsyncStorage do few weeks later
    // const launchedValue = AsyncStorage.getItem("firstLaunchedssss")
    // if(launchedValue.val === null){
    //   AsyncStorage.setItem("firstLaunchedssss",true)
    //   this.setState({isFirstLaunch:true})
    // }else{
    //   this.setState({isFirstLaunch:false})
    // }
  }

  render(){
    // if(this.state.isFirstLaunch){
    //   console.log("First Launch 1",this.state.isFirstLaunch)
    // }else{
    //   console.log("Not First Launch 1",this.state.isFirstLaunch)
    // }
    return (
      // Mine
      // --------------------------------------------------------
      <>
        <IconRegistry icons={EvaIconsPack} /> 
        <ApplicationProvider {...eva} theme={eva.light}>
        <NavigationContainer>
          <Stack.Navigator>
           
            {/* <Stack.Screen name="onBoarding" component={onBoarding} options={{headerShown: false}}/>
            <Stack.Screen name="Login" component={Login} options={{headerShown: false}}/>
            <Stack.Screen name="Register" component={Register} options={{headerShown: false}}/> */}
            {/* combine stack navigator with drawer by using component  */}
            {/* <Stack.Screen name="Home" component={DrawerNavigator} options={{headerShown: false}}/>
            <Stack.Screen name="Search" component={SearchScreen} options={{headerShown: false}}/> */}
            <Stack.Screen name="RestaurantScreen" component={RestaurantScreen} options={{headerShown: false}}/>
            <Stack.Screen name="FoodOption" component={FoodOption} options={{headerShown: false}}/>
          </Stack.Navigator>
        </NavigationContainer>
        </ApplicationProvider>
      </>
      // options={{title: "HomePage"}}
      //  --------------------------------------------------------

    )
  }
}

export default App;
