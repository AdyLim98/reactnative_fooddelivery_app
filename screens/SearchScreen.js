import React,{useState} from 'react';
import { View, StyleSheet, Text } from 'react-native';

import { SearchBar } from 'react-native-elements';
import EIcon from 'react-native-vector-icons/EvilIcons';

const leftIcon = () =>(
    <EIcon
      name="arrow-left"
      size={20}
      color="white"
    />
)
const Search = () =>{
    const [search,setSearch] = useState('')
    return(
        <View>
            <SearchBar
                placeholder="Type Here..."
                onChangeText={(val)=>setSearch(val)}
                value={search}
                searchIcon={(search)?(leftIcon):true}
                // lightTheme={false}
                platform="android"
                // containerStyle={{height:18,backgroundColor:"white",borderColor:"white"}}
                // containerStyle={{height:18}}
                // inputContainerStyle={{width:"100%"}}
                // inputStyle={{width:"100%"}}
            />
        </View>
    )
}

// const styles = StyleSheet.create({

// })

export default Search;