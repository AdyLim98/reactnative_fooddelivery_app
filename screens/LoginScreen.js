import React, {useState} from 'react';
import { SafeAreaView, StyleSheet ,TextInput, Text, View, TouchableOpacity, Button, TouchableHighlight, Image } from 'react-native';
import { Icon } from '@ui-kitten/components';

import auth from '@react-native-firebase/auth';
import { GoogleSignin } from '@react-native-google-signin/google-signin';
import { LoginManager, AccessToken } from 'react-native-fbsdk-next';

// import * as eva from '@eva-design/eva';
// import { ApplicationProvider, IconRegistry,Layout, Text ,Input ,Card,Button,Icon } from '@ui-kitten/components';
// import { EvaIconsPack } from '@ui-kitten/eva-icons';
// import { StyleSheet } from 'react-native'

async function onFacebookButtonPress() {
  // Attempt login with permissions
  const result = await LoginManager.logInWithPermissions(['public_profile', 'email']);
  console.log("Result Facebook:",result)
  if (result.isCancelled) {
    throw 'User cancelled the login process';
  }

  // Once signed in, get the users AccesToken
  const data = await AccessToken.getCurrentAccessToken();

  if (!data) {
    throw 'Something went wrong obtaining access token';
  }

  // Create a Firebase credential with the AccessToken
  const facebookCredential = auth.FacebookAuthProvider.credential(data.accessToken);

  // Sign-in the user with the credential
  return auth().signInWithCredential(facebookCredential);
}

async function getFBUserInfo(){
  const user = await auth().currentUser;
  console.log("User:",user)
}

async function signOutFB(){
  await auth().signOut()
  console.log("Log Out Successful!")
}

async function onGoogleButtonPress() {
    try{
  
        await GoogleSignin.hasPlayServices()
        // Get the users ID token
        const userInfo = await GoogleSignin.signIn();
  
        console.log("Hi123:",userInfo)
        // Create a Google credential with the token
        const googleCredential = auth.GoogleAuthProvider.credential(userInfo.idToken);
  
        // Sign-in the user with the credential
        return auth().signInWithCredential(googleCredential);
    
      }catch(err){
        console.log("Login Error:" , err)
    }
}

async function getCurrentUser(){
    try{
      const user = await GoogleSignin.getCurrentUser();
      console.log("User:",user)
    }catch(err){
      console.log("ERR:",err)
    }
}

async function signOut (){
    try{
      await GoogleSignin.signOut();
      console.log("Sign Out Successful")
    }catch(err){
      console.log(err)
    }
}
  
const Login = ({navigation}) =>{
    const [email,setEmail] = useState('')
    const [password,setPassword] = useState('')

    return(
        <SafeAreaView style={styles.container}>
            <View style={styles.logInContainer}>
                <View><Image source={require('../assets/logo.png')} style={{width:75,height:75}}/></View>
                <Text style={styles.logInFont}>Log in</Text>
            </View>

            <View style={styles.field}>
                <TextInput placeholder="Email" value={email} onChangeText={(e)=>setEmail(e)}
                    style={styles.emailInput} keyboardType="email-address"
                />
              
                <View style={{flexDirection:'row'}}>
                    <TextInput placeholder="Password" value={password} onChangeText={(e)=>setPassword(e)}
                        style={styles.passwordInput} secureTextEntry={true} 
                    />
                </View>

                <Text style={{marginBottom:20}}>Forgot Password ?</Text>

                <TouchableHighlight style={styles.button} disabled={!(email && password)} onPress={()=>{console.log("Pressed")}}>
                    <Text style={{textAlign:'center',color:'white',paddingVertical:10}}>Log In</Text>
                </TouchableHighlight>

                <View style={{marginTop:90}}/>
                
                {/* <View style={{flexDirection:"row"}} > */}
                  <TouchableHighlight style={styles.authButton} onPress={()=>onGoogleButtonPress().then(()=>console.log("Sign In Google Successfully !"))}>
                      <Text style={{textAlign:'center',color:'rgb(176,198,186)',paddingVertical:10}}>Sign In With Google  
                      <View style={{paddingLeft:35}}>
                        <Icon
                          style={styles.icon}
                          fill='#8F9BB3'
                          name='google-outline'
                        />
                      </View>
                      </Text>
                  </TouchableHighlight>
                {/* </View> */}
                
                <TouchableHighlight style={styles.authButton} onPress={()=>{onFacebookButtonPress().then(() => console.log('Signed in with Facebook!'))}}>
                    <Text style={{textAlign:'center',color:'rgb(176,198,186)',paddingVertical:10,marginLeft:-10}}>Sign In With Facebook
                    <View style={{paddingLeft:30}}>
                      <Icon
                        style={styles.icon}
                        fill='#8F9BB3'
                        name='facebook-outline'
                      />
                    </View>
                    </Text>
                </TouchableHighlight>

                {/* <Button title="Sign Out" onPress={()=>signOut().then(() => console.log('Signed Out with Google!'))}></Button> */}
                <View style={{flexDirection:'row'}}>
                    <Text style={{marginHorizontal:10,fontWeight:"200"}}>Not a member ?</Text>
                    <Text style={{fontWeight:"900"}} onPress={()=>{navigation.navigate("Register")}}>Join now</Text>
                </View>
            </View>
        </SafeAreaView>
    
    // UI Kitten
    // --------------------------------------------------------
    // return(  
    //   <>
    //   <IconRegistry icons={EvaIconsPack} />
    //   <ApplicationProvider {...eva} theme={eva.light}>
    //     <Layout style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
    //       {/* <Card> */}
    //         <Text category='h1'>HOME</Text>
    //         {/* <Input placeholder="Email" value={this.state.email} onChangeText={(e)=>{this.setState({email:e})}}/> */}
    //         <Button>Sign In</Button>
    //         {/* <Text>{this.state.email}</Text> */}
    //         <Icon
    //           style={styles.icon}
    //           fill='#8F9BB3'
    //           name='play-circle'
    //         />
    //       {/* </Card> */}
    //     </Layout>
    //   </ApplicationProvider>
    //   </>
    // )
    )
}

const styles = StyleSheet.create({
    container:{
      flexDirection:'column',
      flex:1,
      // padding:20
    },
    logInContainer:{
      flex:1,
      justifyContent:'center',
      alignItems:'center'
    },
    logInFont:{
      fontWeight:'900',
      fontSize:25,
      marginTop:50
    },
    field:{
      flex:2,
      alignItems:'center',
    },
    emailInput:{
      height: 40,
      marginHorizontal: 45,
      borderWidth: 1,
      paddingLeft: 20,
      width:"80%",
      borderRadius:25,
      backgroundColor:"rgb(255,255,255)",
      borderColor:'white',
      marginBottom:20
    },
    passwordInput:{
      height: 40,
      marginHorizontal: 35,
      borderWidth: 1,
      paddingLeft: 20,
      width:"80%",
      borderRadius:25,
      backgroundColor:"rgb(255,255,255)",
      borderColor:'white',
      marginBottom:20,
      flex:1
    },
    button:{
      marginHorizontal: 35,
      borderWidth: 1,
      width:"80%",
      borderRadius:25,
      marginBottom:20,
      borderColor:'white',
      backgroundColor:"rgb(255, 77, 77)",
      // opacity:10
    },
    authButton:{
      marginHorizontal: 35,
      borderWidth: 1,
      width:"80%",
      borderRadius:25,
      marginBottom:10,
      borderColor:'rgb(230, 247, 237)',
      backgroundColor:"rgb(230, 247, 237)",
      // opacity:.9
    },
    icon: {
      width: 15,
      height: 15,
    }
  
});
  
export default Login;