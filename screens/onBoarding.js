import React from 'react';
import { View, Image, Button } from 'react-native'
import Onboarding from 'react-native-onboarding-swiper';

const onBoarding = ({navigation}) =>{
    return(
            <View style={{height:"100%"}}>
                <Onboarding
                    onSkip={()=>{navigation.navigate("Login")}}
                    onDone={()=>{navigation.navigate("Login")}}
                    pages={[
                        {
                            backgroundColor: '#a6e4d0',
                            image: <Image source={require('../assets/onboarding-img1.png')} />,
                            title: 'All Your Favourite',
                            subtitle: 'FoodMan Consists A Lot Of Restaurant And Cuisine',
                        },
                        {
                            backgroundColor: '#fdeb93',
                            image: <Image source={require('../assets/onboarding-img2.png')} />,
                            title: 'Unmatched Reliability',
                            subtitle: 'Experience peace of mind while tracking your order in real time',
                        },
                        {
                            backgroundColor: '#e9bcbe',
                            image: <Image source={require('../assets/onboarding-img3.png')} />,
                            title: 'Discount Voucher Every Month',
                            subtitle: 'Gain and Grab Voucher to enjoy DISCOUNT',
                        }
                    ]}
                />
            </View>

    )
}

export default onBoarding;