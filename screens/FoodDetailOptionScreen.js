import React from 'react';
import { View,Text,StyleSheet,Image,ScrollView } from 'react-native';

import { Icon,Button,Radio,RadioGroup } from '@ui-kitten/components';

const leftIcon = (props) => (
    <Icon {...props} name='arrow-left-outline'/>
);

const FoodOption = ({navigation}) =>{
    
    const [selectedIndex, setSelectedIndex] = React.useState(0);

    return(
        <View>
            {/* <ScrollView> */}
            <View style={{backgroundColor:"rgba(255, 77, 77,0.8)",height:"100%"}}>
                <View style={{height:350}}>
                    <Image source={{uri:"https://cdn.kfc.com.my/images/menu/delivery/delivery_SetA-2pc-Combo.png"}} style={{width:"100%",height:"60%",borderBottomLeftRadius:45,borderBottomRightRadius:45,zIndex:1}} />
                    <Button style={[styles.button,{zIndex:1}]} accessoryLeft={leftIcon} onPress={()=>navigation.goBack()} />
                </View>
                <View style={{marginTop:"-35%",marginHorizontal:15}}>
                    <View style={{flexDirection:"row",justifyContent:"space-between"}}>
                        <Text style={{color:"white",fontWeight:"bold",fontSize:22}}>2-PC Combo</Text>
                        <Text style={{color:"white",fontWeight:"bold",fontSize:13,marginTop:7}}>RM16.99</Text>
                    </View>
                    <Text style={{color:"white",fontSize:10,marginTop:5}}>2-pc Chicken (OR/HS), 1 Coleslaw(R), 1 Whipped Potato (R), choice of 1 drink. Whipped Potato is subject to availability to and maybe replaced with wedges</Text>
                    <View style={{borderBottomColor:"rgba(255,255,255,0.2)",borderBottomWidth:1,marginTop:15}}/>
                    <View style={{marginTop:10}}>
                        <Text style={{color:"white",fontWeight:"bold",fontSize:20}}>Variation</Text>
                        <Text style={{color:"white",fontSize:10,marginLeft:2}}>Select One</Text>
                        <RadioGroup style={{marginTop:10}}
                            selectedIndex={selectedIndex}
                            onChange={index => setSelectedIndex(index)}>
                                <Radio status="warning">{evaProps => <Text {...evaProps} style={{color:"white",marginLeft:10}}>Original Recipe</Text>}</Radio>
                                <Radio status="warning">{evaProps => <Text {...evaProps} style={{color:"white",marginLeft:10}}>Spicy</Text>}</Radio>
                                <Radio status="warning">{evaProps => <Text {...evaProps} style={{color:"white",marginLeft:10}}>Mixed</Text>}</Radio>
                        </RadioGroup>
                    </View>
                    <View style={{borderBottomColor:"rgba(255,255,255,0.2)",borderBottomWidth:1,marginTop:15}}/>
                    <View style={{marginTop:10}}>
                        <Text style={{color:"white",fontWeight:"bold",fontSize:20}}>Add On</Text>
                        <Text style={{color:"white",fontSize:10,marginLeft:2}}>Select Your Extra Favourite Item</Text>
                        <RadioGroup style={{marginTop:10}}
                            selectedIndex={selectedIndex}
                            onChange={index => setSelectedIndex(index)}>
                                <Radio status="warning">{evaProps => <Text {...evaProps} style={{color:"white",marginLeft:10}}>Original Recipe</Text>}</Radio>
                                <Radio status="warning">{evaProps => <Text {...evaProps} style={{color:"white",marginLeft:10}}>Spicy</Text>}</Radio>
                                <Radio status="warning">{evaProps => <Text {...evaProps} style={{color:"white",marginLeft:10}}>Mixed</Text>}</Radio>
                        </RadioGroup>
                    </View>
                </View>
            </View>
            {/* </ScrollView> */}
        </View>
    )
}

const styles = StyleSheet.create({
    button:{
        borderRadius:25,
        width:5,
        height:5,
        backgroundColor:"rgb(211,211,211)",
        borderColor:"rgb(211,211,211)",
        position:"absolute",
        top:10,
        left:10
    },
})

export default FoodOption;