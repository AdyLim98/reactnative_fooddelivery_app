import React from 'react';

import { View, StyleSheet, ScrollView, Image, TouchableHighlight } from 'react-native';
import { Text , Button, Icon  } from '@ui-kitten/components';

import REButton from 'react-native-elements/dist/buttons/Button';
import REIcon from 'react-native-vector-icons/FontAwesome';
import IonIcon from 'react-native-vector-icons/Ionicons';
import FIcon from 'react-native-vector-icons/Feather';
import MAIcon from 'react-native-vector-icons/MaterialCommunityIcons';

const restaurantIcon = (props) => (
    <Icon {...props} name='home-outline'/>
);

const coffeeIcon = () =>(
    <FIcon
      name="coffee"
      size={20}
      color="white"
    />
)

const burgerIcon = () =>(
    <IonIcon
      name="fast-food-outline"
      size={20}
      color="white"
    />
)

const cakeIcon = () =>(
    <MAIcon
      name="cake"
      size={20}
      color="white"
    />
)

{/* <REIcon
      name="arrow-right"
      size={15}
      color="white"
/> */}
const Home = ({navigation}) =>{

    return(
     <View>
        <ScrollView contentContainerStyle={{ paddingBottom: 60 }} showsVerticalScrollIndicator={false}>
            <View>
                <ScrollView horizontal style={{marginVertical:20}}>
                    <View style={{marginHorizontal:15}} opacity={0.7}>
                        <Button style={styles.categoryBttn} accessoryLeft={restaurantIcon}>
                            RESTAURANTS
                        </Button>
                    </View>
                    <View style={{marginHorizontal:15}} opacity={0.7}>
                        <Button style={styles.categoryBttn} accessoryLeft={coffeeIcon}>
                            CAFE
                        </Button>
                    </View>
                    <View style={{marginHorizontal:15}} opacity={0.7}>
                        <Button style={styles.categoryBttn} accessoryLeft={burgerIcon}>
                            Fast Food
                        </Button>
                    </View>
                    <View style={{marginHorizontal:15}} opacity={0.7}>
                        <Button style={styles.categoryBttn} accessoryLeft={cakeIcon}>
                            Desserts
                        </Button>
                    </View>
                </ScrollView>
            </View>
            <View style={styles.homeContainer}>
                <Text style={styles.title}>Your Restaurants</Text>
                <Button style={styles.button} appearance='outline' status='danger' size="tiny">
                    View All
                </Button>
            </View>
            <View>
                <ScrollView horizontal style={{marginTop:10}}>
                    <View style={styles.imageContainer}>
                        {/* width(730px x 456px) */}
                        <TouchableHighlight  underlayColor="disable" onPress={()=>{navigation.navigate("RestaurantScreen")}}>
                        <View>
                            <Image source={{uri:"https://cdn.kfc.com.my/images/menu/delivery/stacker-box-v3.jpg"}} style={{width:180, height:150, marginHorizontal:10,borderRadius:25}} />
                            <View style={styles.restaurantContainer}>
                                <Text style={styles.restaurantName}>KFC (K Mart Muar)</Text>
                                <Text style={styles.restaurantDescription}>Fast Food, Halal</Text>
                                <Text style={styles.restaurantPrice}>RM2.99</Text>
                            </View>
                        </View>
                        </TouchableHighlight>
                        <View style={{marginLeft:-25}}>
                            <Image source={{uri:"https://www.mcdonalds.com.my/microsites/pricereduce/images/mcchicken_m.jpg?id=172690bd5b4467d54c4f"}} style={{width:180, height:150, marginHorizontal:10,borderRadius:25}}/>
                            <View style={styles.restaurantContainer}>
                                <Text style={styles.restaurantName}>Mac Donald (Wetex)</Text>
                                <Text style={styles.restaurantDescription}>Fast Food, Halal</Text>
                                <Text style={styles.restaurantPrice}>RM3.99</Text>
                            </View>
                        </View>
                        <View style={{marginLeft:-40}}>
                            <Image source={require('../assets/pizza.jpg')} style={{width:180, height:150, marginHorizontal:10,borderRadius:25}}/>
                            <View style={styles.restaurantContainer}>
                                <Text style={styles.restaurantName}>Pizza Hut</Text>
                                <Text style={styles.restaurantDescription}>Fast Food, Halal</Text>
                                <Text style={styles.restaurantPrice}>RM4.99</Text>
                            </View>
                        </View>
                        <View>
                            <Image source={require('../assets/sushi.jpg')} style={{width:180, height:150, marginHorizontal:10,borderRadius:25}}/>
                            <View style={styles.restaurantContainer}>
                                <Text style={styles.restaurantName}>Sushi King</Text>
                                <Text style={styles.restaurantDescription}>Japan Food, Halal</Text>
                                <Text style={styles.restaurantPrice}>RM2.99</Text>
                            </View>
                        </View>
                        <View>
                            <Image source={{uri:"https://cdn.kfc.com.my/images/menu/delivery/stacker-box-v3.jpg"}} style={{width:180, height:150, marginHorizontal:10,borderRadius:25}}/>
                            <View style={styles.restaurantContainer}>
                                <Text style={styles.restaurantName}>KFC (K Mart Muar)</Text>
                                <Text style={styles.restaurantDescription}>Fast Food, Halal</Text>
                                <Text style={styles.restaurantPrice}>RM2.99</Text>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
            <View style={styles.homeContainer}>
                <Text style={styles.title}>Popular Food</Text>
                <Button style={styles.button} appearance='outline' status='danger' size="tiny">
                    View All
                </Button>
            </View>
            <View>
                <ScrollView horizontal style={{marginTop:10}}>
                <View style={styles.imageContainer}>
                        {/* width(730px x 456px) */}
                        <View>
                            <Image source={{uri:"https://cdn.kfc.com.my/images/menu/delivery/stacker-box-v3.jpg"}} style={{width:180, height:150, marginHorizontal:10,borderRadius:25}}/>
                            <View style={styles.restaurantContainer}>
                                <Text style={styles.restaurantName}>Stacker Box</Text>
                                <Text style={styles.restaurantDescription}>Fast Food, Halal</Text>
                                <Text style={styles.restaurantPrice}>RM24.00</Text>
                            </View>
                        </View>
                        <View >
                            <Image source={{uri:"https://www.mcdonalds.com.my/microsites/pricereduce/images/mcchicken_m.jpg?id=172690bd5b4467d54c4f"}} style={{width:180, height:150, marginHorizontal:10,borderRadius:25}}/>
                            <View style={styles.restaurantContainer}>
                                <Text style={styles.restaurantName}>Mac Chicken Set</Text>
                                <Text style={styles.restaurantDescription}>Fast Food, Halal</Text>
                                <Text style={styles.restaurantPrice}>RM9.99</Text>
                            </View>
                        </View>
                        <View style={{marginLeft:-10}}>
                            <Image source={require('../assets/pizza.jpg')} style={{width:180, height:150, marginHorizontal:10,borderRadius:25}}/>
                            <View style={styles.restaurantContainer}>
                                <Text style={styles.restaurantName}>Pizza Hut Set</Text>
                                <Text style={styles.restaurantDescription}>Fast Food, Halal</Text>
                                <Text style={styles.restaurantPrice}>RM32.00</Text>
                            </View>
                        </View>
                        <View>
                            <Image source={require('../assets/sushi.jpg')} style={{width:180, height:150, marginHorizontal:10,borderRadius:25}}/>
                            <View style={styles.restaurantContainer}>
                                <Text style={styles.restaurantName}>Salmon Sashimi</Text>
                                <Text style={styles.restaurantDescription}>Japan Food, Halal</Text>
                                <Text style={styles.restaurantPrice}>RM13.99</Text>
                            </View>
                        </View>
                        <View>
                            <Image source={{uri:"https://cdn.kfc.com.my/images/menu/delivery/stacker-box-v3.jpg"}} style={{width:180, height:150, marginHorizontal:10,borderRadius:25}}/>
                            <View style={styles.restaurantContainer}>
                                <Text style={styles.restaurantName}>Stacker Box</Text>
                                <Text style={styles.restaurantDescription}>Fast Food, Halal</Text>
                                <Text style={styles.restaurantPrice}>RM24.00</Text>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
            <View style={styles.homeContainer}>
                <Text style={styles.title}>Special Offers</Text>
                <Button style={styles.button} appearance='outline' status='danger' size="tiny">
                    View All
                </Button>
            </View>
            
            <View>
                <ScrollView horizontal style={{marginTop:10}}>
                <View style={styles.imageContainer}>
                        {/* width(730px x 456px) */}
                        <View>
                            <Image source={{uri:"https://cdn.kfc.com.my/images/menu/delivery/stacker-box-v3.jpg"}} style={{width:180, height:150, marginHorizontal:10,borderRadius:25}}/>
                            <View style={{position:'absolute',bottom:170,right:10,borderTopLeftRadius:25,borderBottomLeftRadius:25,width:50,backgroundColor:"rgb(255, 77, 77)"}}><Text style={{textAlign:"center",color:"white",fontSize:12}}>30%</Text></View>
                            <View style={styles.restaurantContainer}>
                                <Text style={styles.restaurantName}>Stacker Box</Text>
                                <Text style={styles.restaurantDescription}>Fast Food, Halal</Text>
                                <Text style={styles.restaurantPrice}>RM24.00</Text>
                            </View>
                        </View>
                        <View >
                            <Image source={{uri:"https://www.mcdonalds.com.my/microsites/pricereduce/images/mcchicken_m.jpg?id=172690bd5b4467d54c4f"}} style={{width:180, height:150, marginHorizontal:10,borderRadius:25}}/>
                            <View style={{position:'absolute',bottom:170,right:22,borderTopLeftRadius:25,borderBottomLeftRadius:25,width:50,backgroundColor:"rgb(255, 77, 77)"}}><Text style={{textAlign:"center",color:"white",fontSize:12}}>10%</Text></View>
                            <View style={styles.restaurantContainer}>
                                <Text style={styles.restaurantName}>Mac Chicken Set</Text>
                                <Text style={styles.restaurantDescription}>Fast Food, Halal</Text>
                                <Text style={styles.restaurantPrice}>RM9.99</Text>
                            </View>
                        </View>
                        <View style={{marginLeft:-10}}>
                            <Image source={require('../assets/pizza.jpg')} style={{width:180, height:150, marginHorizontal:10,borderRadius:25}}/>
                            <View style={{position:'absolute',bottom:170,right:10,borderTopLeftRadius:25,borderBottomLeftRadius:25,width:50,backgroundColor:"rgb(255, 77, 77)"}}><Text style={{textAlign:"center",color:"white",fontSize:12}}>20%</Text></View>
                            <View style={styles.restaurantContainer}>
                                <Text style={styles.restaurantName}>Pizza Hut Set</Text>
                                <Text style={styles.restaurantDescription}>Fast Food, Halal</Text>
                                <Text style={styles.restaurantPrice}>RM32.00</Text>
                            </View>
                        </View>
                        <View>
                            <Image source={require('../assets/sushi.jpg')} style={{width:180, height:150, marginHorizontal:10,borderRadius:25}}/>
                            <View style={{position:'absolute',bottom:170,right:18,borderTopLeftRadius:25,borderBottomLeftRadius:25,width:50,backgroundColor:"rgb(255, 77, 77)"}}><Text style={{textAlign:"center",color:"white",fontSize:12}}>50%</Text></View>
                            <View style={styles.restaurantContainer}>
                                <Text style={styles.restaurantName}>Salmon Sashimi</Text>
                                <Text style={styles.restaurantDescription}>Japan Food, Halal</Text>
                                <Text style={styles.restaurantPrice}>RM13.99</Text>
                            </View>
                        </View>
                        <View>
                            <Image source={{uri:"https://cdn.kfc.com.my/images/menu/delivery/stacker-box-v3.jpg"}} style={{width:180, height:150, marginHorizontal:10,borderRadius:25}}/>
                            <View style={{position:'absolute',bottom:170,right:10,borderTopLeftRadius:25,borderBottomLeftRadius:25,width:50,backgroundColor:"rgb(255, 77, 77)"}}><Text style={{textAlign:"center",color:"white",fontSize:12}}>40%</Text></View>
                            <View style={styles.restaurantContainer}>
                                <Text style={styles.restaurantName}>Stacker Box</Text>
                                <Text style={styles.restaurantDescription}>Fast Food, Halal</Text>
                                <Text style={styles.restaurantPrice}>RM24.00</Text>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
        </ScrollView>
     </View>
    )
}

const styles = StyleSheet.create({
    homeContainer:{
      flexDirection:"row",
      justifyContent:"space-between",
      marginTop:10
    },
    title:{
      fontWeight:"900",
      marginLeft:"5%"
    },
    categoryBttn:{
        margin: 2,
        backgroundColor:"rgb(255, 77, 77)",
        borderColor:"rgb(255, 77, 77)"
    },
    button: {
      marginRight:"3%"
    },
    imageContainer:{
      flexDirection:"row",
      marginLeft:25,
      marginEnd:90
    },
    restaurantContainer:{
       marginLeft:"10%",
       marginTop:10
    },
    restaurantName:{
       fontWeight:"bold"
    },
    restaurantDescription:{
        fontSize:10
    },
    restaurantPrice:{
        fontWeight:"bold",
        fontSize:12,
    }
});

export default Home;