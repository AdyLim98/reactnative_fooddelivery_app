import React,{useState} from "react";
import { SafeAreaView, StyleSheet ,TextInput, Text, View, TouchableHighlight, Image, Alert } from 'react-native';

function register(navigation){
    Alert.alert(
        "Sign Up Successful !!",
        "Navigate to HomeScreen",
        [
            { text: "OK", onPress: () => navigation.push("Home") }
        ]
    )
    
}

const Register = ({navigation}) =>{
    const [email,setEmail] = useState('')
    const [password,setPassword] = useState('')

    return(
        <SafeAreaView style={styles.container}>
            <View style={styles.logInContainer}>
                <View><Image source={require('../assets/logo.png')} style={{width:75,height:75}}/></View>
                <Text style={styles.logInFont}>Sign Up</Text>
            </View>

            <View style={styles.field}>
                <TextInput placeholder="Email" value={email} onChangeText={(e)=>setEmail(e)}
                    style={styles.emailInput} keyboardType="email-address"
                />
              
                <View style={{flexDirection:'row'}}>
                    <TextInput placeholder="Password" value={password} onChangeText={(e)=>setPassword(e)}
                        style={styles.passwordInput} secureTextEntry={true} 
                    />
                </View>

                <TouchableHighlight style={styles.button} onPress={()=>register(navigation)}>
                    <Text style={{textAlign:'center',color:'white',paddingVertical:10}}>Register</Text>
                </TouchableHighlight>

                <View style={{marginTop:90}}/>

            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container:{
      flexDirection:'column',
      flex:1,
      marginTop:"30%"
      // padding:20
    },
    logInContainer:{
      flex:1,
      justifyContent:'center',
      alignItems:'center'
    },
    logInFont:{
      fontWeight:'900',
      fontSize:25,
      marginTop:50
    },
    field:{
      flex:2,
      alignItems:'center',
    },
    emailInput:{
      height: 40,
      marginHorizontal: 45,
      borderWidth: 1,
      paddingLeft: 20,
      width:"80%",
      borderRadius:25,
      backgroundColor:"rgb(255,255,255)",
      borderColor:'white',
      marginBottom:20
    },
    passwordInput:{
      height: 40,
      marginHorizontal: 35,
      borderWidth: 1,
      paddingLeft: 20,
      width:"80%",
      borderRadius:25,
      backgroundColor:"rgb(255,255,255)",
      borderColor:'white',
      marginBottom:20,
      flex:1
    },
    button:{
      marginTop:30,
      marginHorizontal: 35,
      borderWidth: 1,
      width:"80%",
      borderRadius:25,
      marginBottom:20,
      borderColor:'white',
      backgroundColor:"rgb(255, 77, 77)",
      // opacity:10
    },
  
});
  
export default Register;