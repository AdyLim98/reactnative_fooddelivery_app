import React, { useState } from 'react';
import { View, Text, Image, StyleSheet, ScrollView, Modal, Pressable, Alert, TouchableHighlight } from 'react-native';

import { Button, Icon, Input } from '@ui-kitten/components';
import AntIcon from 'react-native-vector-icons/AntDesign';

const leftIcon = (props) => (
    <Icon {...props} name='arrow-left-outline'/>
);

const RestaurantScreen = ({navigation}) =>{
    
    const [modalVisible,setModalVisible] = useState(false)
    const [quantity,setQuantity] = useState(0)
    return(
        <View>
            <View style={{marginBottom:10,height:350}}>
                <Image source={{uri:"https://media-cdn.tripadvisor.com/media/photo-s/1b/99/44/8e/kfc-faxafeni.jpg"}} style={{width:"100%",height:"55%"}} />
                <Button style={styles.button} accessoryLeft={leftIcon} onPress={()=>navigation.goBack()} />
                <Text style={{color:"white",fontSize:25,fontWeight:"bold",position:"absolute",top:"20%",left:"35%"}}>KFC Muar</Text>
                <Text style={{position:"absolute",top:"30%",left:"37%",color:"white",fontSize:12,fontWeight:"bold"}}>  
                    Halal , Fast Food
                </Text>
                <Text style={{position:"absolute",top:"35%",left:"44%",color:"white",fontSize:12,fontWeight:"bold"}}>  
                    <AntIcon
                            name="staro"
                            size={15}
                            color="white"/>4.4
                </Text>
            </View>

            {/* Modal details (Inside UI/UX) */}
           <Modal animationType="slide" transparent={true} visible={modalVisible}
                    onRequestClose={()=>{
                        Alert.alert("Modal Closed")
                        setModalVisible(!modalVisible)
                    }}
            >
                <View style={{flex:1,justifyContent:"flex-end"}}>

                        <Image source={{uri:"https://cdn.kfc.com.my/images/menu/delivery/delivery-cheesy-wedges(L).png"}} style={{width:"100%",height:280,borderTopLeftRadius:25,borderTopRightRadius:25,borderBottomLeftRadius:25,borderBottomRightRadius:25,zIndex:1,postion:"absolute",bottom:-20}}/>
            
                        <View style={{backgroundColor:"rgb(255, 77, 77)",height:250}}>
                            <Text style={{textAlign:"center",color:"white",marginTop:25,fontSize:20,fontWeight:"bold"}}>2pc Combo</Text>
                            <Text style={{textAlign:"center",color:"white",fontSize:15,fontWeight:"bold"}}>RM6.99</Text>
                            <View style={{flexDirection:"row",justifyContent:"center",marginTop:15,marginBottom:5}}>
                                <Icon
                                    style={styles.icon}
                                    fill='white'
                                    name='plus-circle-outline'
                                    animation="Zoom"
                                    onPress={()=>{setQuantity(quantity+1)}}
                                />
                                <Text style={{color:"white",marginTop:5,marginHorizontal:5}}>{quantity}</Text>
                                <Icon
                                    style={styles.icon}
                                    fill='white'
                                    name='minus-circle-outline'
                                    onPress={()=>{(quantity>0) ? setQuantity(quantity-1) : setModalVisible(false)}}
                                />
                            </View>
                            <Input
                                multiline={true}
                                textStyle={{ minHeight: 15 }}
                                placeholder='Special Instruction'
                                placeholderTextColor="lightgray"
                                style={{marginHorizontal:10,marginTop:10,borderColor:"white"}}
                            />
                            <View style={{flexDirection:"row",justifyContent:"space-around",marginTop:10}}>
                                <Button style={styles.buttonInModal} onPress={()=>{setModalVisible(false)}}  appearance='outline' status='danger'>
                                    CANCEL
                                </Button>
                                <Button style={styles.buttonInModal}  appearance='outline' status='danger'>
                                    Add To Cart
                                </Button>
                            </View>   
                            
                        </View>
    
                </View>
            </Modal>
            
            <ScrollView style={{marginTop:"-40%",height:520}} contentContainerStyle={{ paddingBottom: 50 }} showsVerticalScrollIndicator={false}>
                    {/* Navigate to option screen if got OPTION */}
                    <TouchableHighlight underlayColor="disable" onPress={()=>navigation.navigate("FoodOption")}>
                        <View style={{marginBottom:10}}>
                            <View style={{flex:1,flexDirection:"row",justifyContent:"space-between",marginHorizontal:10,backgroundColor:"rgb(255, 77, 77)"}}>
                                <View style={{marginVertical:10,marginLeft:10,marginRight:5,flex:2}}>
                                    <Text style={{fontWeight:"bold",fontSize:18,color:"white"}}>2-pc Combo</Text>
                                    <Text numberOfLines={1} style={{marginTop:10,color:"white"}}>2-pc Chickens (OR/HS) ,1 coke ,1 coleslaw  </Text>
                                    <Text style={{fontWeight:"bold",fontSize:15,color:"white"}}>RM16.99</Text>
                                </View>
                                <View style={{flex:1}}>
                                    <Image source={{uri: "https://cdn.kfc.com.my/images/menu/delivery/delivery_SetA-2pc-Combo.png"}} style={{marginLeft:20,borderRadius:5,marginTop:5,marginRight:5,width:80,height:80}}/>
                                </View>
                            </View>
                        </View>
                    </TouchableHighlight>

                    <View style={{marginBottom:10}}>
                        {/* Activate modal when press */}
                        <TouchableHighlight underlayColor="disable" onPress={()=>setModalVisible(true)}>
                            <View style={{flex:1,flexDirection:"row",justifyContent:"space-between",marginHorizontal:10,backgroundColor:"rgb(255, 77, 77)"}}>
                                <View style={{marginVertical:10,marginLeft:10,marginRight:5,flex:2}}>
                                    <Text style={{fontWeight:"bold",fontSize:18,color:"white"}}>Cheezy Wedges (L)</Text>
                                    <Text numberOfLines={1} style={{marginTop:10,color:"white"}}>12 Cheezy Wedges</Text>
                                    <Text style={{fontWeight:"bold",fontSize:15,color:"white"}}>RM6.99</Text>
                                </View>
                                <View style={{flex:1}}>
                                    <Image source={{uri: "https://cdn.kfc.com.my/images/menu/delivery/delivery-cheesy-wedges(L).png"}} style={{marginLeft:20,borderRadius:5,marginTop:5,marginRight:5,width:80,height:80}}/>
                                </View>
                            </View>
                        </TouchableHighlight>
                    </View>

                    <View style={{marginBottom:10}}>
                        <View style={{flex:1,flexDirection:"row",justifyContent:"space-between",marginHorizontal:10,backgroundColor:"rgb(255, 77, 77)"}}>
                            <View style={{marginVertical:10,marginLeft:10,marginRight:5,flex:2}}>
                                <Text style={{fontWeight:"bold",fontSize:18,color:"white"}}>9-pc Nuggets</Text>
                                <Text numberOfLines={1} style={{marginTop:10,color:"white"}}>9-pc hot spicy nuggets,2 barbecue sauce</Text>
                                <Text style={{fontWeight:"bold",fontSize:15,color:"white"}}>RM16.99</Text>
                            </View>
                            <View style={{flex:1}}>
                                <Image source={{uri: "https://cdn.kfc.com.my/images/menu/delivery/9pcs-nuggets-alacarte-spicy-bbq.jpg"}} style={{marginLeft:20,borderRadius:5,marginTop:5,marginRight:5,width:80,height:80}}/>
                            </View>
                        </View>
                    </View>
                    <View style={{marginBottom:10}}>
                        <View style={{flex:1,flexDirection:"row",justifyContent:"space-between",marginHorizontal:10,backgroundColor:"rgb(255, 77, 77)"}}>
                            <View style={{marginVertical:10,marginLeft:10,marginRight:5,flex:2}}>
                                <Text style={{fontWeight:"bold",fontSize:18,color:"white"}}>Loaded Potato Bowl (L)</Text>
                                <Text numberOfLines={1} style={{marginTop:10,color:"white"}}>Loaded Potato Bowl(L)</Text>
                                <Text style={{fontWeight:"bold",fontSize:15,color:"white"}}>RM7.99</Text>
                            </View>
                            <View style={{flex:1}}>
                                <Image source={{uri: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRhBMF-tCmwIiQAXMh5s1Tqefzes0oe4tHIjw&usqp=CAU"}} style={{marginLeft:20,borderRadius:5,marginTop:5,marginRight:5,width:80,height:80}}/>
                            </View>
                        </View>
                    </View>
                    <View style={{marginBottom:10}}>
                        <View style={{flex:1,flexDirection:"row",justifyContent:"space-between",marginHorizontal:10,backgroundColor:"rgb(255, 77, 77)"}}>
                            <View style={{marginVertical:10,marginLeft:10,marginRight:5,flex:2}}>
                                <Text style={{fontWeight:"bold",fontSize:18,color:"white"}}>Coleslaw (L)</Text>
                                <Text numberOfLines={1} style={{marginTop:10,color:"white"}}>Coleslaw (L)  </Text>
                                <Text style={{fontWeight:"bold",fontSize:15,color:"white"}}>RM7.99</Text>
                            </View>
                            <View style={{flex:1}}>
                                <Image source={{uri: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQmdNSUs6WC-7OMB-8mCjfNKNQkT4D8CHAGLw&usqp=CAU"}} style={{marginLeft:20,borderRadius:5,marginTop:5,marginRight:5,width:80,height:80}}/>
                            </View>
                        </View>
                    </View>
                    <View style={{marginBottom:10}}>
                        <View style={{flex:1,flexDirection:"row",justifyContent:"space-between",marginHorizontal:10,backgroundColor:"rgb(255, 77, 77)"}}>
                            <View style={{marginVertical:10,marginLeft:10,marginRight:5,flex:2}}>
                                <Text style={{fontWeight:"bold",fontSize:18,color:"white"}}>3-pc Combo</Text>
                                <Text numberOfLines={1} style={{marginTop:10,color:"white"}}>2-pc Chickens (OR/HS) asdasd as d as da sd a sd a sd  </Text>
                                <Text style={{fontWeight:"bold",fontSize:15,color:"white"}}>RM20.49</Text>
                            </View>
                            <View style={{flex:1}}>
                                <Image source={{uri: "https://cdn.kfc.com.my/images/menu/delivery/delivery_SetB-3pc-Combo.png"}} style={{marginLeft:20,borderRadius:5,marginTop:5,marginRight:5,width:80,height:80}}/>
                            </View>
                        </View>
                    </View>
                    <View style={{marginBottom:10}}>
                        <View style={{flex:1,flexDirection:"row",justifyContent:"space-between",marginHorizontal:10,backgroundColor:"rgb(255, 77, 77)"}}>
                            <View style={{marginVertical:10,marginLeft:10,marginRight:5,flex:2}}>
                                <Text style={{fontWeight:"bold",fontSize:18,color:"white"}}>2-pc Rice Combo</Text>
                                <Text numberOfLines={1} style={{marginTop:10,color:"white"}}>2-pc Chickens (OR/HS) asdasd as d as da sd a sd a sd  </Text>
                                <Text style={{fontWeight:"bold",fontSize:15,color:"white"}}>RM16.99</Text>
                            </View>
                            <View style={{flex:1}}>
                                <Image source={{uri: "https://cdn.kfc.com.my/images/menu/delivery/2pc-rice-combo-v2.png"}} style={{marginLeft:20,borderRadius:5,marginTop:5,marginRight:5,width:80,height:80}}/>
                            </View>
                        </View>
                    </View>
                    <View style={{marginBottom:10}}>
                        <View style={{flex:1,flexDirection:"row",justifyContent:"space-between",marginHorizontal:10,backgroundColor:"rgb(255, 77, 77)"}}>
                            <View style={{marginVertical:10,marginLeft:10,marginRight:5,flex:2}}>
                                <Text style={{fontWeight:"bold",fontSize:18,color:"white"}}>Signature Box - Classic</Text>
                                <Text numberOfLines={1} style={{marginTop:10,color:"white"}}>2-pc Chickens (OR/HS) asdasd as d as da sd a sd a sd  </Text>
                                <Text style={{fontWeight:"bold",fontSize:15,color:"white"}}>RM19.99</Text>
                            </View>
                            <View style={{flex:1}}>
                                <Image source={{uri: "https://cdn.kfc.com.my/images/menu/delivery/signature-box-v4.jpg"}} style={{marginLeft:20,borderRadius:5,marginTop:5,marginRight:5,width:80,height:80}}/>
                            </View>
                        </View>
                    </View>
                    <View style={{marginBottom:10}}>
                        <View style={{flex:1,flexDirection:"row",justifyContent:"space-between",marginHorizontal:10,backgroundColor:"rgb(255, 77, 77)"}}>
                            <View style={{marginVertical:10,marginLeft:10,marginRight:5,flex:2}}>
                                <Text style={{fontWeight:"bold",fontSize:18,color:"white"}}>Zinger Cheezy Combo</Text>
                                <Text numberOfLines={1} style={{marginTop:10,color:"white"}}>Zinger Cheezy Burger ,French Fries, Coke(L)</Text>
                                <Text style={{fontWeight:"bold",fontSize:15,color:"white"}}>RM16.70</Text>
                            </View>
                            <View style={{flex:1}}>
                                <Image source={{uri: "https://cdn.kfc.com.my/images/menu/delivery/zinger-cheezy-fries-delivery-v2.jpg"}} style={{marginLeft:20,borderRadius:5,marginTop:5,marginRight:5,width:80,height:80}}/>
                            </View>
                        </View>
                    </View>
            </ScrollView>
            
        </View>
    )
}

const styles = StyleSheet.create({
    button:{
        borderRadius:25,
        width:5,
        height:5,
        backgroundColor:"rgb(211,211,211)",
        borderColor:"rgb(211,211,211)",
        position:"absolute",
        top:10,
        left:10
    },
    icon: {
        width: 32,
        height: 32,
    },
    buttonInModal:{
        margin:2,
        backgroundColor:"white",
        
    }
    // groupRestaurantName:{
    //     position:"absolute",
    //     top:"20%",
    //     left:"35%"
    // },
    // menu:{
    //     flexDirection:row,
    //     justifyContent:''
    // }
})
export default RestaurantScreen;